 \documentclass[9pt]{beamer}
\setbeamertemplate{section in toc}[sections numbered]

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
% Use roboto Font (recommended)
%\usepackage[sfdefault]{roboto}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

%~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\usetheme[block=fill]{metropolis}
% ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
\usepackage{xcolor}
\definecolor{lightblue}{rgb}{0,0.5,0.7}
\definecolor{lightred}{rgb}{0.95,0.2,0.2}
\definecolor{midgreen}{rgb}{.1,.8,.1}
% Maths

%% AMS stuff for maths
\usepackage{amsmath,amsfonts,amssymb}
\usepackage{stmaryrd}
\usepackage{centernot}
%% bold-face in mathmode
\usepackage{bm}
%% tikz
\usepackage{tikz}
\usepackage{tkz-graph}
\usepackage{pgfplots}
\usepackage{adjustbox}
\usepackage[
  backend=biber,
  style=numeric,
  maxbibnames=10,
  minalphanames=3,
  ]{biblatex}

  \addbibresource{./presentation.bib}
\let\footnotesize\tiny
\title{An evaluation game for topological $\mu$-calculus}
\author{Iwan Quémerais}
\institute{ÉNS de Lyon}
\date{July 26 2023}

\begin{document}
\maketitle
% Parity games (5 min)
\begin{frame}{Outline}
  \tableofcontents
\end{frame}
\section{Parity games}
\begin{frame}{Board games}
  \begin{minipage}[l][.5\textheight][l]{.45\linewidth}
    \begin{itemize}
    \item Played on a graph
    \item Two players: $\exists$(Éloise) and $\forall$(Abélard)
    \item At each turn one player has to move the token depending on its current position
    \item<6-> If one player has no move available, they lose
    \item<11-> Infinite games are possible, some winning conditions must be defined for these cases
    \end{itemize}
  \end{minipage}
  \begin{minipage}[r][\textheight][c]{.45\linewidth}
    \begin{tikzpicture}
      \SetGraphUnit{2}
      \Vertex[x=2,y=2]{A}
      \Vertex[x=4,y=2]{C}
      \Vertex[x=6,y=2]{E}
      \Vertex[x=5,y=3]{G}
      \Vertex[x=7,y=3]{H}
      \tikzset{VertexStyle/.append style={rectangle}};
      \Vertex[x=3,y=1]{B}
      \Vertex[x=5,y=1]{D}
      \Vertex[x=7,y=1]{F}

      \tikzset{EdgeStyle/.style = {->}}
      \Edges(A,B,C,D,E,F)
      \Edges(A, C)
      \Edges(E, G)
      \Edges(E, H)
      \Edges(D, G)
      \tikzset{EdgeStyle/.style = {->,bend right = 40}};
      \Edges(G, A)

      \tikzset{VertexStyle/.append style={circle, fill = gray}};
      \only<1,11>{\Vertex[x=2,y=2]{A}}
      \only<3>{\Vertex[x=4,y=2]{C}}
      \only<5,7,9>{\Vertex[x=6,y=2]{E}}
      \only<6>{\Vertex[x=7,y=3]{H}}
      \only<10>{\Vertex[x=5,y=3]{G}}
      \tikzset{VertexStyle/.append style={rectangle, fill = gray}};
      \only<2>{\Vertex[x=3,y=1]{B}}
      \only<4>{\Vertex[x=5,y=1]{D}}
      \only<8>{\Vertex[x=7,y=1]{F}}

      \node[] at (3,0) { $\bigcirc$: $\exists$'s move };
      \node[] at (3,-.5) { $\square$: $\forall$'s move };
    \end{tikzpicture}
  \end{minipage}
\end{frame}
\begin{frame}{Parity games}
  \begin{minipage}[l][.5\textheight][l]{.45\linewidth}
    \begin{itemize}
    \item Each position has a color corresponding to a player
    \item The positions are ordered ($A\leq B \leq \dots \leq H$)
    \item<2-> If the greatest position that occurs infinitely many times is red the $\exists$ wins
    \item<3-> If the greatest position that occurs infinitely many times is blue the $\forall$ wins
    \end{itemize}
  \end{minipage}
  \begin{minipage}[r][\textheight][c]{.45\linewidth}
    \begin{tikzpicture}
      \SetGraphUnit{2}
      \tikzset{VertexStyle/.style={circle, fill = lightred}}
      \Vertex[x=4,y=2]{C}
      \Vertex[x=6,y=2]{E}
      \Vertex[x=5,y=3]{G}
      \tikzset{VertexStyle/.style={circle, fill = lightblue}}
      \Vertex[x=2,y=2]{A}
      \Vertex[x=7,y=3]{H}
      \tikzset{VertexStyle/.style={rectangle,fill = lightred}};
      \Vertex[x=3,y=1]{B}
      \Vertex[x=5,y=1]{D}
      \tikzset{VertexStyle/.style={circle, fill = lightblue}}
      \Vertex[x=7,y=1]{F}

      \tikzset{EdgeStyle/.style = {->}}
      \Edges(A,B,C,D,E,F)
      \Edges(A, C)
      \Edges(E, G)
      \Edges(E, H)
      \Edges(D, G)
      \tikzset{EdgeStyle/.style = {->,bend right = 40}};
      \Edges(G, A)
      \tikzset{EdgeStyle/.style = {->,bend left = 40}};
      \Edges(F, D)

      \tikzset{EdgeStyle/.style = {->, thick, color = midgreen}};
      \only<2>{\Edges(A,B,C,D,E,G)
        \tikzset{EdgeStyle/.append style = {bend right = 40}};
        \Edges(G, A)
      }
      \only<3>{\Edges(A,B,C,D,E,F)
        \tikzset{EdgeStyle/.append style = {bend left = 40}};
        \Edges(F, D)
      }
    \end{tikzpicture}
  \end{minipage}
\end{frame}
%Fixpoint game (3 min)
\section{Fixpoint game}
\begin{frame}
  \begin{block}{way-below}
    In a lattice $(L,\sqsubseteq)$, we say that $l'\ll l$ if for all directed set $\mathcal{D} \subseteq L$ such that $l \sqsubseteq \bigsqcup \mathcal{D}$, there exist $d\in \mathcal{D}$ such that $l'\sqsubseteq d$
  \end{block}
  \begin{block}{Continuous lattice}
    A complete lattice $(L,\sqsubseteq)$ is continuous if for all $l\in L$ , $l= \bigcup \{ l'\ll l\}$
  \end{block}
\end{frame}
\begin{frame}{Definition of the fixpoint game}
  Let $f$ a monotone function, and lets consider the equation : \[ x =_\eta f(x) \] with $\eta \in \{\mu,\nu\}$. Let $(L,\sqsubseteq)$ a continuous lattice and $\mathcal{B}$ a basis of $L$.\\
  The fixpoint game defined in   \footfullcite{DBLP:journals/pacmpl/BaldanKMP19}
  \begin{center}    
    \begin{tabular}[h]{|c|c|c|}
      \hline
      Position & Player & Moves \\
      \hline
      $(b,\exists)$ & $\exists$ & $l$ such that $b\sqsubseteq f(l)$ \\
      $(l,\forall)$ & $\forall$ & $b'$ such that $b'\ll l$\\
      \hline
    \end{tabular}
  \end{center}
  If $\eta = \mu$ then $\forall$ wins infinite plays, and if $\eta = \nu$ then $\exists$ wins infinite plays
  \begin{block}{Characterisation of the fixpoint equations}
    Let $x=_\eta f(x)$ an equation with $\eta \in \{ \mu,\nu\}$ and $f$ monotone. Let $u$ the solution of $x=_\eta f(x)$, and $b\in \mathcal{B}$.
    \[ b\sqsubseteq u \Leftrightarrow \exists \text{ has a winning strategy from the starting position }b\]    
  \end{block}
\end{frame}
\begin{frame}{Example of a play}
  \begin{center}    
    \begin{tabular}[h]{|c|c|c|}
      \hline
      Position & Player & Moves \\
      \hline
      $(b,\exists)$ & $\exists$ & $l$ such that $b\sqsubseteq f(l)$ \\
      $(l,\forall)$ & $\forall$ & $b'$ such that $b'\ll l$\\
      \hline
    \end{tabular}
  \end{center}
  For the equation $x=_\mu f(x)$ with solution $u$ , from the starting position $b\sqsubseteq u$ $\exists$ can win in finitely many moves :
  \begin{tabular}[h]{|c|c|c|}
    \hline
    Player & Move & remark \\
    \hline
    $\exists$ & $l_1= u$ & $ f(u) = u = \bigsqcup_{n\in \mathbb{N}} f^n(\bot)$ \\
    $\forall$ & $ b_1\ll l_1$ & there exist $n\in \mathbb{N}$ s.t. $b_1 \sqsubseteq f^n (\bot)$ \\
    $\exists$ & $ l_2 = f^{n-1}(\bot)$ & \\
    $\forall$ & $ b_2\ll l_2$&\\
    $\vdots$ & $\vdots$ & \\
    $\exists$ & $l_m = \bot$ & \\
    $\forall$ & - & has no possible moves\\
    \hline
  \end{tabular}
\end{frame}
\begin{frame}{Applying the game to a topology}
  \begin{block}{Locally compact space}
      A topological space $(X,\Omega)$ is locally compact if for all $x\in X$ and for all $U\in \Omega$ such that $x\in U$, there exist $K\subseteq U$ a compact neighbourhood of $x$.
  \end{block}
  \begin{block}{Locally compact spaces and continuous lattices}
    Let $(X,\Omega)$ be a locally compact space, then $(\Omega,\subseteq)$ is a continuous lattice.
  \end{block}
  \begin{block}{Remark on meets and joins}
    For the equation $x=_\mu f(x)$ with solution $u$ : \[ u= \bigsqcup_{n\in \mathbb{N}}f^n(\bot) = \bigcup_{n\in \mathbb{N}} f^n (\bot) \]
    but for the equation $x=_\nu f(x)$ with solution $u$ : \[ u = \bigsqcap_{n\in \mathbb{N}} f^n(\top) = \left( \bigcap_{n\in \mathbb{N}} f^n(\top) \right)^\circ \neq \bigcap_{n\in \mathbb{N}} f^n(\top)\]    
  \end{block}
\end{frame}
% Topological mu calculus (2 min)
\section{Topological $\mu$-calculus}
\begin{frame}{$\mu$-calculus}
  \begin{align*}
    \mathcal{L}_\mu \ni \phi\hspace{1em} ::=&p,p\in \textsc{Prop} | \phi\wedge \phi | \phi\vee \phi | \bot | \top| \diamondsuit \phi | \Box \phi |\\
                                            & \mu p.\phi(p,q_1,\dots,q_n) | \nu p. \phi(p,q_1,\dots,q_n)
  \end{align*}
    Let $V$ be some valuation and $\mathcal{R} \subseteq X\times X$ a relation.
  We define the semantics $\llbracket \phi \rrbracket_V$ on $\mathcal{L}_\mu$ of a formula $\phi$ by induction :
\[\begin{array}[h]{rlrl}
    \llbracket p\rrbracket_V &:= V(p) &&\\
    \llbracket \psi_1\wedge \psi_2 \rrbracket_V &:= \llbracket \psi_1 \rrbracket_V \cap \llbracket \psi_2 \rrbracket_V & \llbracket \psi_1 \vee \psi_2 \rrbracket_V &:= \llbracket \psi_1 \rrbracket_V \cup \llbracket \psi_2 \rrbracket_V \\
    \llbracket \bot \rrbracket_V&:=\emptyset & \llbracket \top \rrbracket_V &:= X \\
    \llbracket \diamondsuit \psi \rrbracket_V &:= \{ x\in X | \mathcal{R}[x] \cap \llbracket \psi \rrbracket_V \neq \emptyset \} &  \llbracket \Box \psi\rrbracket_V &:= \{ x\in X | \mathcal{R}[x] \subseteq \llbracket \psi \rrbracket_V \} \\
    \llbracket \mu p. \psi  \rrbracket_V & := \operatorname{lfp}(\psi_p^V) & \llbracket \nu p.\psi \rrbracket_V & := \operatorname{gfp}(\psi_p^V)^\circ
  \end{array}\]
\end{frame}
\begin{frame}{$\mu$-calculus on opens}
  \begin{minipage}[h][.3\textheight][c]{\linewidth}
\[\begin{array}[h]{rlrl}
    \llbracket p\rrbracket_V &:= V(p) &&\\
    \llbracket \psi_1\wedge \psi_2 \rrbracket_V &:= \llbracket \psi_1 \rrbracket_V \cap \llbracket \psi_2 \rrbracket_V & \llbracket \psi_1 \vee \psi_2 \rrbracket_V &:= \llbracket \psi_1 \rrbracket_V \cup \llbracket \psi_2 \rrbracket_V \\
    \llbracket \bot \rrbracket_V&:=\emptyset & \llbracket \top \rrbracket_V &:= X \\
    \llbracket \diamondsuit \psi \rrbracket_V &:= \{ x\in X | \mathcal{R}[x] \cap \llbracket \psi \rrbracket_V \neq \emptyset \} &  \llbracket \Box \psi\rrbracket_V &:= \{ x\in X | \mathcal{R}[x] \subseteq \llbracket \psi \rrbracket_V \} \\
    \llbracket \mu p. \psi  \rrbracket_V & := \operatorname{lfp}(\psi_p^V) & \llbracket \nu p.\psi \rrbracket_V & := \operatorname{gfp}(\psi_p^V)^\circ
  \end{array}\]    
  \end{minipage}
  \begin{minipage}[h][.55\textheight][c]{\linewidth}
    Let $(X,\Omega)$ be a topological space, we need\footfullcite{MKHS} :
    \begin{itemize}
    \item<1-> $V : \operatorname{Prop}\to \Omega$
    \item<2-> $\mathcal{R}^{-1}[U]$ is open for each open set $U\subseteq X$
    \item<3-> $\mathcal{R}^{-1}[F]$ is closed for each closed set $F\subseteq X$
    \end{itemize}
    \only<4>{\begin{block}{\texttt{MLKH}-space}
      A modal locally compact Hausdorff space (\texttt{MLKH}-space) is a triplet $(X,\Omega,\mathcal{R})$ such that $(X,\Omega)$ is locally compact Hausdorff and $\mathcal{R}\in X\times X$ is a continuous relation.
    \end{block}}
  \end{minipage}
\end{frame}

% Evaluation game (5 min)
\section{Evaluation game}
\begin{frame}{Some definitions}
  Let $\mathcal{B}= \{ K^\circ | K \subseteq X \text{ is a non-empty compact }\}$, $\mathcal{B}$ is a subbasis of the locally compact Hausdorff space $(X,\Omega)$
  \begin{block}{the way-below relation}
    In an \texttt{MLKH}-space, let $U,V\in \Omega$, we say that $U$ is way below $V$ if $\overline{U} \subseteq V$ and $\overline{U}$ is compact. We write $U\ll V$
  \end{block}
  \begin{block}{ Evaluation game}
    An evaluation game is a parity game which characterizes some property over the valuation of some formula, in our case we want to define a game $\mathcal{G}$ such that for all $b\in \mathcal{B}$, $\phi \in \mathcal{L}_\mu$ :
    \[ b\ll \llbracket \phi \rrbracket_V \Leftrightarrow (\phi,b) \in \operatorname{Win}_\exists (\mathcal{G}(\phi,V)) \]
  \end{block}
\end{frame}
\begin{frame}{Evaluation game}
   \begin{adjustbox}{width =\linewidth, center}
    \begin{tabular}[c]{|c|c|c|}
        \hline
  Position & Player & Possible moves\\
      \hline \hline
          $(p,b), p\in \operatorname{FVar}(\phi)$ and $b\centernot\ll V(p)$ & $\exists$ & $\emptyset$ \\
    $(p,b), p\in \operatorname{FVar}(\phi)$ and $b\ll V(p)$ & $\forall$ & $\emptyset$ \\
    $(\psi_1\wedge \psi_2, b)$ & $\forall$ & $\{ (\psi_1,b), (\psi_2,b)\}$\\
    $(\psi_1\vee \psi_2,b)$ & $\exists$ & $\left\{ \langle(\psi_1,b'),(\psi_2,b'')\rangle  | b'\cup b''= b \right\}\cup \{ (\psi_1,b),(\psi_2,b)\}$ \\
    $\langle (\psi_1, b'), (\psi_2,b'') \rangle $ & $\forall$ & $\{ (\psi_1, b'), (\psi_2,b'') \} $ \\
    $(\diamondsuit\psi,b)$ & $\exists$ & $\{ (\psi,b')| b' \in \mathcal{B}, b\subseteq \mathcal{R}^{-1}[b']\}$ \\
    $(\Box\psi,b)$ & $\exists$& $\{(\psi,l,\Box) | l\in \Omega, \mathcal{R}[\overline{b}]\subseteq l\}$\\
    $(\psi,l,\Box)$ & $\forall$& $\{(\psi,b') | b'\in \mathcal{B}, b'\ll l \}$\\
    $(p,b), p\in \operatorname{BVar}(\phi), \phi @ p = \eta p.\psi$& $\exists$ & $\{(\psi, b)\}$\\
  \hline
    \end{tabular}
  \end{adjustbox}
  A finite full play of $\mathcal{G}(\phi,V)$ is lost by the player who got stuck at the end of the play. An infinite full play $\pi$ depends on the maximal element of $\operatorname{Inf}(\pi,\phi)$ with respect to $\leq_\phi$, let $p= \operatorname{max}(\operatorname{Inf}(\pi,\phi))$ if $\phi @ p$ is of the form $\nu p. \psi$ then $\exists$ wins, otherwise $\forall$ wins.
\end{frame}

\begin{frame}{Example of a play}
     \begin{adjustbox}{width =.8\linewidth, center}
    \begin{tabular}[c]{|c|c|c|}
        \hline
  Position & Player & Possible moves\\
      \hline \hline
          $(p,b), p\in \operatorname{FVar}(\phi)$ and $b\centernot\ll V(p)$ & $\exists$ & $\emptyset$ \\
    $(p,b), p\in \operatorname{FVar}(\phi)$ and $b\ll V(p)$ & $\forall$ & $\emptyset$ \\
    $(\psi_1\vee \psi_2,b)$ & $\exists$ & $\left\{ \langle(\psi_1,b'),(\psi_2,b'')\rangle  | b'\cup b''= b \right\}\cup \{ (\psi_1,b),(\psi_2,b)\}$ \\
    $\langle (\psi_1, b'), (\psi_2,b'') \rangle $ & $\forall$ & $\{ (\psi_1, b'), (\psi_2,b'') \} $ \\
    $(\Box\psi,b)$ & $\exists$& $\{(\psi,l,\Box) | l\in \Omega, \mathcal{R}[\overline{b}]\subseteq l\}$\\
    $(\psi,l,\Box)$ & $\forall$& $\{(\psi,b') | b'\in \mathcal{B}, b'\ll l \}$\\
    $(p,b), p\in \operatorname{BVar}(\phi), \phi @ p = \eta p.\psi$& $\exists$ & $\{(\psi, b)\}$\\
  \hline
    \end{tabular}
  \end{adjustbox}
  \begin{minipage}[l][.6\textheight][l]{.35\linewidth}
    \begin{itemize}
    \item $\mathcal{R} = \{ (x,x^3) | x\in \mathbb{C}\}$
    \item $\phi = \mu p . q\vee \Box p$      
    \only<1>{\item current move : $(\mu p. q \vee \Box p, b_1)$}
    \only<2>{\item current move : $(q \vee \Box p, b_1)$}
    \only<3-4>{\item current move : $(\Box p, b_1)$}
    \only<5>{\item current move : $(p,l_1,\Box)$}
    \only<6>{\item current move : $(\mu p. q \vee \Box p, b_2)$}
    \only<7>{\item current move : $(q \vee \Box p, b_2)$}
    \only<8-9>{\item current move : $(\Box p, b_2)$}
    \only<10>{\item current move : $(p,l_2,\Box)$}
    \only<11>{\item current move : $(\mu p. q \vee \Box p, b_3)$}
    \only<12>{\item current move : $(q \vee \Box p, b_3)$}
    \only<13>{\item current move : $(q, b_3)$}
    \end{itemize}
  \end{minipage}
  \begin{minipage}[r][.6\textheight][c]{.6\linewidth}
    \begin{tikzpicture}[scale = .8]
      \begin{axis}[
    trig format plots=rad,
    axis equal,
    legend pos = north west]
    \addplot[domain = 0:2*pi, samples = 50, midgreen] ({cos(x)/2}, {sin(x)/2}); \addlegendentry{$V(q)$}
    \only<{1-4}>{
      \addplot[domain = 0:2*pi, samples = 50, lightblue] ({.4*cos(x) + .5}, {.4*sin(x)}); \addlegendentry{$b_1$}
    }
    \only<4-5>{
      \addplot[domain = 0:2*pi, samples = 100, dotted, thick, lightred] ({8/125*cos(x)^3 - 24/125*cos(x)*sin(x)^2 + 6/25*cos(x)^2 - 6/25* sin(x)^2 +3*cos(x)/10 + 1/8 }, {8/125*cos(x)^2 *sin(x) - 8/125*sin(x)^3 + 12/25*cos(x)*sin(x) + 3*sin(x)/10}); \addlegendentry{$\mathcal{R}[\overline{b_1}]$}
    }
    \only<5>{
      \addplot[domain = 0:2*pi, samples = 50] ({cos(x)*.51 +0.25},{sin(x)*.51}); \addlegendentry{$l_1$}
    }
    \only<6-8>{
      \addplot[domain = 0:2*pi, samples = 50, lightblue] ({cos(x)/2 +0.25},{sin(x)/2}); \addlegendentry{$b_2$}
    }
    \only<9-10>{
      \addplot[domain = 3*pi/2:5*pi/2, samples = 100, dotted, thick, lightred] ({cos(3*x)/8 + 3*cos(2*x)/16 + 3*cos(x)/32 +1/64}, {sin(3*x)/8 +3*sin(2*x)/16 +3*sin(x)/32});
      \addlegendentry{$\mathcal{R}[\overline{b_2}]$}
    }
    \only<10>{
      \addplot[domain = 0:2*pi, samples = 50] ({.37*cos(x) + 0.1}, {.37*sin(x)}); \addlegendentry{$l_1$}
    }
    \only<11-13>{
      \addplot[domain = 0:2*pi, samples = 50, lightblue] ({.36*cos(x) +0.1},{.36*sin(x)}); \addlegendentry{$b_3$}
    }
    \addplot[domain = 0:2*pi, samples = 50, dotted] ({cos(x)}, {sin(x)}); 
      \end{axis}
    \end{tikzpicture}
  \end{minipage}
\end{frame}
% Bisimulation (2 min)
\section{Bisimulations}

\begin{frame}{Definitions}
  \begin{block}{Continuous bisimulation}
    Let $(X_1,\Omega_1,\mathcal{R}_1)$ and $(X_2,\Omega_2,\mathcal{R}_2)$ \textsl{MLKH-spaces} with valuations $V_1: \operatorname{Prop} \to \Omega_1$ and $V_2: \operatorname{Prop}\to \Omega_2$. A relation $Z\subseteq X_1\times X_2$ is called a continuous bisimulation if and only if $Z$ is a Kripke bisimulation and if for all opens $U\in \Omega_2, Z^{-1}[U]\in \Omega_1$ and for all $C$ closed in $(X_2,\Omega_2)$, $Z^{-1}[C]$ is closed in $(X_1,\Omega_1)$.
  \end{block}
  \begin{block}{Way-below on bisimulations}
    Let $(X_1,\Omega_1,\mathcal{R}_1)$ and $(X_2,\Omega_2,\mathcal{R}_2)$ \textsl{MLKH-spaces} with valuations $V_1: \operatorname{Prop} \to \Omega_1$ and $V_2: \operatorname{Prop}\to \Omega_2$. Let $Z$ be a continuous bisimulation, we define $\ll_Z \in \Omega_1\times \Omega_2$ as :
    \[ U \ll_Z V \Leftrightarrow \forall x\in \overline{U}, \exists y\in V, (x,y)\in Z \Leftrightarrow \overline{U} \subseteq Z^{-1}[V] \]
  \end{block}    
\end{frame}
\begin{frame}{A few results}
  \begin{block}{Bisimulation between bases}
    Let $(X_1,\Omega_1,\mathcal{R}_1)$ and $(X_2,\Omega_2,\mathcal{R}_2)$ \textsl{MLKH-spaces} with valuations $V_1: \operatorname{Prop} \to \Omega_1$ and $V_2: \operatorname{Prop}\to \Omega_2$ and let $Z$ be a continuous bisimulation between these spaces. Let $\mathcal{B}_1 = \{U\in \Omega_1  | \overline{U}\text{ is a non empty compact }\}$ and $\mathcal{B}_2 = \{U\in \Omega_2 | \overline{U}\text{ is a non empty compact }\}$. Let $b_1\in \mathcal{B}_1, b_2\in \mathcal{B}_2$, then for all $\phi \in \mathcal{L}_\mu$ :
    \[ b_1 \ll_Z b_2 \wedge b_2 \ll \llbracket \phi \rrbracket_{V_2} \Rightarrow b_1 \ll \llbracket \phi \rrbracket_{V_1} \]
  \end{block}
  \begin{block}{Bisimulation between topologies}
      Let $(X_1,\Omega_1,\mathcal{R}_1)$ and $(X_2,\Omega_2,\mathcal{R}_2)$ \textsl{MLKH-spaces} with valuations $V_1: \operatorname{Prop} \to \Omega_1$ and $V_2: \operatorname{Prop}\to \Omega_2$ and let $Z$ be a continuous bisimulation between these spaces. Let $U\in \Omega_1,V\in \Omega_2$, then for all $\phi \in \mathcal{L}_\mu$ :
      \[ U \subseteq Z^{-1}[V] \wedge V\subseteq \llbracket \phi \rrbracket_{V_2} \Rightarrow U \subseteq \llbracket \phi \rrbracket_{V_1} \]
  \end{block}
\end{frame}
\end{document}