\babel@toc {english}{}\relax 
\contentsline {section}{\numberline {1}Parity games}{2}{section.1}%
\contentsline {subsection}{\numberline {1.1}Board games}{2}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Parity games}{3}{subsection.1.2}%
\contentsline {section}{\numberline {2}Existing works}{4}{section.2}%
\contentsline {subsection}{\numberline {2.1}Fixpoint game on continuous lattices}{4}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Games for a topological fixpoint logic}{5}{subsection.2.2}%
\contentsline {section}{\numberline {3}Fixpoint game on a topological space}{6}{section.3}%
\contentsline {subsection}{\numberline {3.1}Chosing the right topology}{6}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Application to the fixpoint game}{7}{subsection.3.2}%
\contentsline {section}{\numberline {4}$\mu $-calculus on opens}{8}{section.4}%
\contentsline {section}{\numberline {5}Evaluation game for the $\mu $-calculus}{10}{section.5}%
\contentsline {section}{\numberline {6}Bisimulation}{14}{section.6}%
\contentsline {section}{\numberline {7}Conclusion}{16}{section.7}%
